using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Rigidbody rb;
    private float destroytime = 15.0f;
    private float movespeed = 0.15f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * movespeed;

        destroytime -= Time.deltaTime;
        if (destroytime < 0)
        {
            Destroy(gameObject);
            Destroy(this);
        }       
    }
}
