using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightHandBallController : MonoBehaviour
{
    // Start is called before the first frame update
    public Bullet bullet;

    float shotInterval = 100000f;
    float shotTimer;

    void Start()
    {
        
    }
        
    void Update()
    {
        Debug.Log($"{shotTimer}, {Time.deltaTime.ToString()}");
        shotTimer -= Time.deltaTime;
        if (shotTimer < 0) {
            Instantiate(bullet, transform.position, transform.rotation);
            shotTimer = shotInterval;
        }
    }
}
