using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour
{

    public float movespeed = 0.004f;
    Renderer renderer;
    public int health = 4;

    // Start is called before the first frame update
    void Start()
    {
        this.renderer = GetComponent<Renderer>();
        renderer.material.color = new Color(0, 255, 0);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;
        position.z -= movespeed;
        transform.position = position;
    }

    void FixedUpdate()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.GetType().ToString());
        Bullet bullet = other.gameObject.GetComponent<Bullet>();
        if(bullet != null)
        {
            Destroy(other);
            health -= 1;               
            switch (health)
            {
                case 3:
                    renderer.material.color = new Color(50, 210, 0);
                    break;
                case 2:
                    renderer.material.color = new Color(100, 100, 0);
                    break;
                case 1:
                    renderer.material.color = new Color(255, 0, 0);
                    break;
                case 0:
                    Destroy(gameObject);                    
                    break;
            }
        }
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    Bullet bullet = collision.gameObject.GetComponent<Bullet>();
    //    if (bullet != null)
    //    {
    //        Destroy(gameObject);
    //    }
    //}

    //void OnTriggerExit(Collider other)
    //{
    //    RightHandBallController hand = other.gameObject.GetComponent<RightHandBallController>();
    //    if (hand != null)
    //    {
    //        Destroy(gameObject);
    //    }
    //}
}
