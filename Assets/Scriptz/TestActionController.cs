using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class TestActionController : MonoBehaviour
{
    private ActionBasedController controller;
    public Bullet bullet;

    float shotInterval = 1f;
    float shotTimer;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<ActionBasedController>();

        controller.selectAction.action.performed += SelectActionPerformed;
        controller.activateAction.action.performed += ActivatePerformed;        
    }

    private void ActivatePerformed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        //Debug.Log("Activate Button is pressed lmao");
    }

    private void SelectActionPerformed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        //Debug.Log("Select Button is pressed lmao");
    }

    // Update is called once per frame
    void Update()
    {
        shotTimer -= Time.deltaTime;
        if (shotTimer < 0 && controller.activateAction.action.IsPressed())
        {
            Instantiate(bullet, transform.position, transform.rotation);
            shotTimer = shotInterval;
        }
    }
}
