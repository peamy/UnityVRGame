using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour
{
    public SpiderController spider;
    public SpawnPointController LeftSpawn;
    public SpawnPointController CenterSpawn;
    public SpawnPointController RightSpawn;
    public TowerContoller Tower;

    private Quaternion leftRotation;
    private Quaternion rightRotation;
    private Quaternion centerRotation;

    float spawnInterval = 4f;
    float spawnTimer;

    // Start is called before the first frame update
    void Start()
    {

        centerRotation = Quaternion.LookRotation(Tower.transform.position - CenterSpawn.transform.position, Vector3.up);
        leftRotation = Quaternion.LookRotation(Tower.transform.position - LeftSpawn.transform.position, Vector3.up);
        rightRotation = Quaternion.LookRotation(Tower.transform.position - RightSpawn.transform.position, Vector3.up);
    }

    // Update is called once per frame
    void Update()
    {

        spawnTimer -= Time.deltaTime;
        if (spawnTimer < 0)
        {
            Instantiate(spider, CenterSpawn.transform.position, centerRotation);
            Instantiate(spider, LeftSpawn.transform.position, leftRotation);
            Instantiate(spider, RightSpawn.transform.position, rightRotation);
            spawnTimer = spawnInterval;
        }
    }
}
