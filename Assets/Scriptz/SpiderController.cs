using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderController : MonoBehaviour
{
    private Animator anim;
    public float movespeed = 0.01f;

    public GameObject leg1;
    public GameObject leg2;
    public GameObject leg3;
    public int health { get; set; } = 4;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.Play("run");
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += movespeed * transform.forward;
    }

    private void OnTriggerEnter(Collider other)
    {
        Bullet bullet = other.gameObject.GetComponent<Bullet>();
        if (bullet != null)
        {
            Destroy(other.gameObject);
            health -= 1;
            Debug.Log(health);
            switch (health)
            {
                case 3:
                    Destroy(leg1);
                    break;
                case 2:
                    Destroy(leg2);
                    break;
                case 1:
                    Destroy(leg3);
                    break;
                case 0:
                    Destroy(gameObject);
                    break;
            }
        }
    }
}
